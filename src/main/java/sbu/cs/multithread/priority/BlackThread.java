package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class BlackThread extends ColorThread {

    private static final String MESSAGE = "hi blues, hi whites";

    CountDownLatch countDownLatch ;

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    public BlackThread(CountDownLatch cd){
        this.countDownLatch = cd ;
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        printMessage();
        countDownLatch.countDown();
    }
}
