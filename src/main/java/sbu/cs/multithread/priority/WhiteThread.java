package sbu.cs.multithread.priority;

import java.util.concurrent.CountDownLatch;

public class WhiteThread extends ColorThread {

    private static final String MESSAGE = "hi back blacks, hi back blues";

    CountDownLatch countDownLatch ;

    public WhiteThread(CountDownLatch cd) {
       this.countDownLatch = cd ;
    }

    void printMessage() {
        super.printMessage(new Message(this.getClass().getName(), getMessage()));
    }

    @Override
    String getMessage() {
        return MESSAGE;
    }

    @Override
    public void run() {
        // call printMessage
        printMessage();
        countDownLatch.countDown();
    }
}
