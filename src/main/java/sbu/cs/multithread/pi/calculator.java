package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class calculator implements Runnable{

    private final int counter;
    private static BigDecimal two = new BigDecimal(2, new MathContext(100, RoundingMode.HALF_DOWN));

    public calculator(int counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        BigDecimal bigDecimal = new BigDecimal(2, new MathContext(100)) ;
        BigDecimal bigDecimal1 = new BigDecimal(1).divide(new BigDecimal(16).pow(counter)) ;
        BigDecimal bigDecimal2 = new BigDecimal(4).divide(new BigDecimal(8).multiply(new BigDecimal(counter)).add(new BigDecimal(1))) ;
        BigDecimal bigDecimal3 = new BigDecimal(2).divide(new BigDecimal(8).multiply(new BigDecimal(counter)).add(new BigDecimal(4))) ;
        BigDecimal bigDecimal4 = new BigDecimal(1).divide(new BigDecimal(8).multiply(new BigDecimal(counter)).add(new BigDecimal(5))) ;
        BigDecimal bigDecimal5 = new BigDecimal(1).divide(new BigDecimal(8).multiply(new BigDecimal(counter)).add(new BigDecimal(6))) ;

        bigDecimal = bigDecimal1.multiply(bigDecimal2.subtract(bigDecimal3).subtract(bigDecimal4).subtract(bigDecimal5));
        Taylor.endComputation(bigDecimal);

    }
}
